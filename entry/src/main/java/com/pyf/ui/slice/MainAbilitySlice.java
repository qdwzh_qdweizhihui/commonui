package com.pyf.ui.slice;

import com.pyf.common.ui.BaseAbilitySlice;
import com.pyf.ui.ResourceTable;
import com.pyf.ui.presenter.AbilitySliceProvider;
import com.pyf.ui.presenter.MainAbilitySlicePresenter;

import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends BaseAbilitySlice implements AbilitySliceProvider {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        // 使用MVP模式，将业务逻辑放到presenter中
        new MainAbilitySlicePresenter(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
