package com.pyf.ui.fraction.home;

import com.pyf.common.ui.BaseFraction;
import com.pyf.ui.ResourceTable;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */

public class AntiviralFraction extends BaseFraction {

    @Override
    public int getUIContent() {
        return ResourceTable.Layout_fraction_antiviral;
    }

    @Override
    public void initComponent() {

    }
}
