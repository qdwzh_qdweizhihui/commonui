package com.pyf.ui.refresh;

/**
 * 下拉刷新接口
 *
 * @author 裴云飞
 * @date 2020/12/27
 */
public interface IRefresh {

    /**
     * 下拉刷新的时候是否禁止滚动
     *
     * @param disableRefreshScroll
     */
    void setDisableRefreshScroll(boolean disableRefreshScroll);

    /**
     * 刷新完成
     */
    void refreshFinish();

    /**
     * 设置下拉刷新监听
     *
     * @param listener 监听
     */
    void setRefreshListener(RefreshListener listener);

    /**
     * 设置下拉刷新的头部组件
     *
     * @param headOverComponent 下拉刷新的头部组件
     */
    void setRefreshOverComponent(HeadOverComponent headOverComponent);

    interface RefreshListener {

        /**
         * 告诉调用者，此时正在刷新
         */
        void onRefresh();

        /**
         * 是否允许刷新
         *
         * @return
         */
        boolean enableRefresh();
    }
}
