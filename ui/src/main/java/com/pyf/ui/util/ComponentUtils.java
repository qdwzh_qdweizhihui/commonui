package com.pyf.ui.util;

import java.util.ArrayDeque;
import java.util.Deque;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */

public class ComponentUtils {

    public static <T> T findTypeView(ComponentContainer group, Class<T> cls) {
        if (group == null) {
            return null;
        }
        Deque<Component> deque = new ArrayDeque<>();
        deque.add(group);
        while (!deque.isEmpty()) {
            Component node = deque.removeFirst();
            if (cls.isInstance(node)) {
                return cls.cast(node);
            } else if (node instanceof ComponentContainer) {
                ComponentContainer container = (ComponentContainer) node;
                for (int i = 0, count = container.getChildCount(); i < count; i++) {
                    deque.add(container.getComponentAt(i));
                }
            }
        }
        return null;
    }

}
