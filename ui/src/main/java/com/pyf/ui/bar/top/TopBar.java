package com.pyf.ui.bar.top;

import com.pyf.ui.ResourceTable;
import com.pyf.ui.bar.common.IBar;
import com.pyf.ui.util.TextUtils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 顶部导航栏中单个条目的封装，由于目前并不知道泛型的具体类型，所以泛型直接使用？
 *
 * @author 裴云飞
 * @date 2020/12/26
 */
public class TopBar extends DirectionalLayout implements IBar<TopBarInfo<?>> {

    /**
     * 当前条目所对应的数据
     */
    private TopBarInfo<?> barInfo;
    private Image mImage;
    private Text mBarName;
    private Component mIndicator;

    public TopBar(Context context) {
        this(context, null);
    }

    public TopBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TopBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_bar_top, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mBarName = (Text) findComponentById(ResourceTable.Id_name);
        mIndicator = findComponentById(ResourceTable.Id_indicator);
    }

    @Override
    public void setBarInfo(TopBarInfo<?> info) {
        barInfo = info;
        inflateInfo(false, true);
    }

    private void inflateInfo(boolean selected, boolean init) {
        if (barInfo.barType == TopBarInfo.BarType.IMAGE) {
            if (init) {
                mImage.setVisibility(VISIBLE);
                mBarName.setVisibility(HIDE);
            }
            if (selected) {
                mImage.setPixelMap(barInfo.selectedImage);
                mIndicator.setVisibility(VISIBLE);
            } else {
                mImage.setPixelMap(barInfo.defaultImage);
                mIndicator.setVisibility(HIDE);
            }
        } else if (barInfo.barType == TopBarInfo.BarType.TEXT) {
            if (init) {
                mImage.setVisibility(HIDE);
                mBarName.setVisibility(VISIBLE);
                if (!TextUtils.isEmpty(barInfo.name)) {
                    mBarName.setText(barInfo.name);
                }
            }
            if (selected) {
                mBarName.setTextColor(new Color(parseColor(barInfo.tintColor)));
                mIndicator.setVisibility(VISIBLE);
            } else {
                mBarName.setTextColor(new Color(parseColor(barInfo.defaultColor)));
                mIndicator.setVisibility(HIDE);
            }
        }
    }

    private int parseColor(Object color) {
        if (color instanceof String) {
            return Color.getIntColor((String) color);
        } else {
            return (int) color;
        }
    }
    @Override
    public void resetHeight(int height) {
        DirectionalLayout.LayoutConfig config = (LayoutConfig) getLayoutConfig();
        config.height = height;
        setLayoutConfig(config);
    }

    @Override
    public void onBarSelectedChange(int index, TopBarInfo<?> preInfo, TopBarInfo<?> nextInfo) {
        if (preInfo != barInfo && nextInfo != barInfo) {
            return;
        }
        if (preInfo == nextInfo) {
            return;
        }
        if (preInfo == barInfo) {
            inflateInfo(false, false);
        } else {
            inflateInfo(true, false);
        }
    }

    public TopBarInfo<?> getBarInfo() {
        return barInfo;
    }

    public Image getImage() {
        return mImage;
    }

    public Text getBarName() {
        return mBarName;
    }

    public Component getIndicator() {
        return mIndicator;
    }
}
