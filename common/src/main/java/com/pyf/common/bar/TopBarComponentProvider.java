package com.pyf.common.bar;

import com.pyf.ui.bar.top.TopBarInfo;

import java.util.List;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */
public class TopBarComponentProvider extends BarComponentProvider {

    private List<TopBarInfo<?>> mInfoList;

    public TopBarComponentProvider(FractionManager fractionManager, List<TopBarInfo<?>> infoList) {
        super(fractionManager);
        mInfoList = infoList;
    }

    @Override
    public Fraction getFraction(int position) {
        try {
            return (Fraction) mInfoList.get(position).fraction.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mInfoList.size();
    }

}
